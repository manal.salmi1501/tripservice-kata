package org.craftedsw.tripservicekata.trip;

import java.util.ArrayList;
import java.util.List;

import org.craftedsw.tripservicekata.exception.UserNotLoggedInException;
import org.craftedsw.tripservicekata.user.User;
import org.springframework.beans.factory.annotation.Autowired;

public class TripService {

    @Autowired
    private TripDAO tripDAO;

    public List<Trip> getTripsByUser(User friend, User loggedUser) throws UserNotLoggedInException {

        validate(loggedUser);

        if (friend.isFriendWith(loggedUser)) {
            return tripByUser(friend);
        }
        return noTrips();

    }

    private ArrayList<Trip> noTrips() {
        return new ArrayList<>();
    }

    private void validate(User loggedUser) {
        if (loggedUser == null) {
            throw new UserNotLoggedInException();
        }
    }

    private List<Trip> tripByUser(User user) {
        return tripDAO.tripsByUser(user);
    }
}

package org.craftedsw.tripservicekata.trip;

import org.craftedsw.tripservicekata.exception.CollaboratorCallException;
import org.craftedsw.tripservicekata.user.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class TripDAOTest {

    @Test
    public void should_throw_exception() {
        assertThrows(CollaboratorCallException.class,
                () -> {
                    TripDAO tripDAO = new TripDAO();

                    tripDAO.tripsByUser(new User());
                });
    }

}

package org.craftedsw.tripservicekata.trip;

import org.craftedsw.tripservicekata.exception.UserNotLoggedInException;
import org.craftedsw.tripservicekata.user.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TripServiceTest {

    private static final User GUEST = null;
    private static final User USER = new User();
    private static final User REGISTRED_USER = new User();
    private static final User ANOTHER_USER = new User();
    private static final Trip TRIP = new Trip();

    @Mock
    private TripDAO tripDAO;

    @InjectMocks
    private TripService tripService = new TripService();

    @Test()
    public void should_throw_exception_when_user_is_not_logged() {

        assertThrows(UserNotLoggedInException.class,
                () -> {
                    tripService.getTripsByUser(USER, GUEST);
                });
    }

    @Test
    public void should_not_return_any_trips_when_user_is_not_friends() {
        User friend = new User();
        friend.addFriend(ANOTHER_USER);
        friend.addTrip(TRIP);
        List<Trip> tripsByUser = tripService.getTripsByUser(friend, REGISTRED_USER);

        assertTrue(tripsByUser.size() == 0);
    }

    @Test
    public void should_return_trip_when_user_is_friend() {
        User friend = new User();
        friend.addFriend(REGISTRED_USER);
        friend.addTrip(TRIP);

        when(tripDAO.tripsByUser(friend)).thenReturn(Arrays.asList(TRIP));

        List<Trip> tripsByUser = tripService.getTripsByUser(friend, REGISTRED_USER);

        assertTrue(tripsByUser.size() == 1);
    }
}

package org.craftedsw.tripservicekata.user;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserTest {

    @Test
    public void should_return_false_if_user_is_not_friend_with_another_user(){
        User another = new User();
        User user = new User();
        boolean isFriend = user.isFriendWith(another);
        assertFalse(isFriend);
    }

    @Test
    public void should_return_true_if_user_is_friend_with_another_user(){
        User another = new User();
        User user = new User();
        user.addFriend(another);
        boolean isFriend = user.isFriendWith(another);
        assertTrue(isFriend);
    }
}
